<?php

class MR_Scroll_Helper_Data extends Mage_Core_Helper_Abstract {
    public function findProductList($layout)
    {
        $page = $layout->getBlock('product_list');

        if (!$page){
            $page = $layout->getBlock('search_result_list');
        }

        if (!$page) {
            $page = $layout->getBlock('catalogsearch_advanced_result');
        }

        return $page;
    }
}