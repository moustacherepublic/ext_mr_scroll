<?php
/**
 * @copyright   Copyright (c) 2009-2011 Amasty (http://www.amasty.com)
 */ 
class MR_Scroll_Model_Observer
{
    public function handleLayoutRender()
    { 	
        $layout = Mage::getSingleton('core/layout');
        if (!$layout)
            return;
        
        $isAJAX = Mage::app()->getRequest()->getParam('mr_scroll', false);
        $isAJAX = $isAJAX && Mage::app()->getRequest()->isXmlHttpRequest();
        if (!$isAJAX)
            return;
        
        $layout->removeOutputBlock('root');    
        
        $page = Mage::helper('mr_scroll')->findProductList($layout);
        if (!$page) {
            return;
        }

        $container = $layout->createBlock('core/template', 'mr_scroll_container');

        $keepToolbar = Mage::app()->getRequest()->getParam('mr_keep_toolbar', false);
        if($keepToolbar){
            $container->setData('page', $page->toHtml());
        }
        else{
            $toolBar = $page->getToolbarHtml();
            $container->setData('page', str_replace($toolBar, '', $page->toHtml()));
        }

        $layout->addOutputBlock('mr_scroll_container', 'toJson');
    }
}