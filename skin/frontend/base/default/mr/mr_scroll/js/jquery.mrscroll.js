;(function($){
    $.mrscroll = function(options){
        var settings = $.extend({}, $.mrscroll.defaults, options),
            pageCount,
            totalCount,
            limit,
            columnCount,
            requestedPages,
            requestQueue,
            pagesOffset,
            canUpdateHash = true
            ;

        var methods = {
            init: function(){
                $(".toolbar-bottom, .pager").hide();
                methods.removeSorterOnChangeAttribute();
                methods.initAjaxSorter();

                pageCount = methods.getPageCount();
                totalCount = methods.getTotalCount();
                limit = methods.getLimit();
                columnCount = methods.getColumnCount();
                requestedPages = [];
                requestQueue = [];
                pagesOffset = [];

                if(pageCount < 2){
                    return;
                }

                $(window).load(function(){
                    methods.createFakeElements();
                    methods.initPagesOffset();
                    methods.initMoreButton();
                    methods.initFirstPage();
                    //scroll to offset for first visit or refresh
//                    setTimeout(function(){
//                        methods.scrollToOffset();
//                    }, 500);
                });

                //disable updating hash after user click  a link
                window.onbeforeunload = function(){
                    canUpdateHash = false;
                }

                $(window).scroll(function(event){
                    var offset = $(window).scrollTop(),
                        currentPage = methods.calculateCurrentPage(offset);
//                    methods.updateHash(offset, currentPage);
                    methods.updateRequestQueue(currentPage);

                    methods.loadContent();
                });

                //change column
                if(settings.oneColButton !== null && settings.twoColButton !== null){
                    $('body').on('click', settings.oneColButton, function(){
                        methods.reInitPagesOffset();
                    });
                    $('body').on('click', settings.twoColButton, function(){
                        methods.reInitPagesOffset();
                    });
                }

                //window change size or orientation
                $(window).on('resize', function(){
                    methods.reInitPagesOffset();
                });
                $(window).on('orientationchange', function(){
                    methods.reInitPagesOffset();
                });

            },

            removeSorterOnChangeAttribute: function(){
                $('.sorter .sort-by .input-sm').removeAttr('onchange');
            },

            //make sorter ajax
            initAjaxSorter: function(){
                //change order type
                $('body').on('change', '.sorter .sort-by .input-sm', function(event){
                    $('.' + settings.divClass).append('<div class="mrscroll-mask"/>');
                    var requestUrl = $(this).val();
                    methods.ajaxSorterRequest(requestUrl);
                });

                //click order direction
                $('body').on('click', '.sorter .sort-by a', function(event){
                    event.preventDefault();
                    $('.' + settings.divClass).append('<div class="mrscroll-mask"/>');
                    var requestUrl = $(this).attr('href');
                    methods.ajaxSorterRequest(requestUrl);
                });
            },

            ajaxSorterRequest: function(url){
                url = url.replace(/&?is_mrnav=1/g, '');
                $.ajax({
                    type: 'get',
                    url: url,
                    data: {"mr_scroll" : 1, "mr_keep_toolbar" : 1},
                    dataType: 'json',
                    success: function(response){
                        if($('.' + settings.divClass).length > 1){
                            $('.' + settings.divClass).not(':first').remove();
                        }
                        $('.' + settings.divClass).eq(0).replaceWith(response.page);
                        methods.reInit();
                    }
                });
            },

            createFakeElements: function(){

                var lastPageItemCount = totalCount % limit == 0 ? limit : totalCount % limit,
                    leftItemCount = lastPageItemCount,
                    html = '';
                //first create from page 2 to pageCount-1
                for(var p=2; p <= pageCount-1; p++){
                    html += '<div class="' + settings.divClass + ' fake">';
                    if(settings.mode == 'grid'){
                        for(var u=1; u<= Math.ceil(limit / columnCount); u++){
                            html += '<ul class="products-grid'+ (u==1 ? ' first' : '') + (u==Math.ceil(limit / columnCount) ? ' last' : '') +'">';
                                for(var l=1; l<=columnCount; l++){
                                    html += '<li class="item fake'+ (l==1 ? ' first' : '') + (l==columnCount ? ' last' : '') +'"></li>';
                                }
                            html += settings.loaderHtml;
                            html += '</ul>'
                        }
                    }
                    else{
                        html += '<ol class="products-list" id="products-list">';
                        for(var l=1; l<= limit; l++){
                            html += '<li class="item fake'+ (l==1 ? ' first' : '') + (l==limit ? ' last' : '') +'"></li>';
                        }
                        html += settings.loaderHtml;
                        html += '</ol>';
                    }
                    html += '</div>';
                }

                //then create last page
                html += '<div class="' + settings.divClass + ' fake">';
                if(settings.mode == 'grid'){
                    var ulCount = Math.ceil(lastPageItemCount / columnCount);
                    for(var u=1; u<=ulCount ; u++){
                        html += '<ul class="products-grid'+ (u==1 ? ' first' : '') + (u==ulCount && ulCount>=2 ? ' last' : '') +'">';
                        var liCount = Math.min(columnCount, leftItemCount);
                        for(var l=1; l<=liCount; l++, leftItemCount--){
                            html += '<li class="item fake'+ (l==1 ? ' first' : '') + (l==liCount && liCount>=2 ? ' last' : '') +'"></li>';
                        }
                        html += settings.loaderHtml;
                        html += '</ul>'
                    }
                }
                else{
                    html += '<ol class="products-list" id="products-list">';
                    for(var l=1; l<= Math.min(limit, lastPageItemCount); l++){
                        html += '<li class="item fake'+ (l==1 ? ' first' : '') + (l==Math.min(limit, lastPageItemCount) ? ' last' : '') +'"></li>';
                    }
                    html += settings.loaderHtml;
                    html += '</ol>';
                }
                html += '</div>';

                $('.' + settings.divClass).after(html);
                methods.setFakeSectionHeight();
                if(settings.mode == 'grid'){
                    decorateGeneric($$('ul.products-grid'), ['odd','even','first','last'])
                }
            },

            setFakeSectionHeight: function(){
                var firstSectionHeight = $('.' + settings.divClass).eq(0).height(),
                    toolbarHeight = Math.floor($('.' + settings.divClass).eq(0).find('.toolbar').eq(0).outerHeight(true));//include margin

                if($('.' + settings.divClass).eq(0).find('.toolbar').is(':visible')){
                    $('.' + settings.divClass + '.fake').not(':last').height(firstSectionHeight - toolbarHeight);
                }
                else{
                    $('.' + settings.divClass + '.fake').not(':last').height(firstSectionHeight);
                }
            },

            resetSectionHeight: function(){
                $('.' + settings.divClass).not('.fake').css('height', 'auto');
            },

            initPagesOffset: function(){
                var windowHeight = $(window).height();
                $('.' + settings.divClass).each(function(){
                    pagesOffset.push($(this).offset().top - windowHeight);
                });
            },

            initMoreButton: function(){
                if(!settings.autoLoad && !methods.getCanLoadContent()){
                    $('.' + settings.divClass + '.fake').hide();
                    $('.' + settings.divClass).eq(0).append(settings.moreBtnHtml);
                    $('body').on('click', '#mr-scroll-more', function(){
                        $(this).remove();
                        $('.' + settings.divClass + '.fake').show();
                        Mage.Cookies.domain = window.location.host;
                        Mage.Cookies.set('mr_load_more_clicked', 1);
                        $(window).scrollTop($(window).scrollTop() + 1);//trigger load content
                    });
                }
            },

            getCanLoadContent: function(){
                return settings.autoLoad || Mage.Cookies.get('mr_load_more_clicked');
            },

            scrollToOffset: function(){
                if(window.location.hash){
                    $(window).scrollTop(/offset=\d+/.exec(window.location.hash)[0].split('=')[1]);
                }
            },

            initFirstPage: function(){
                if(window.location.hash && /p=\d+/.test(window.location.hash) && methods.getCanLoadContent()){
                    var page = parseInt(/p=\d+/.exec(window.location.hash)[0].split('=')[1]);

                    for(var p=2; p<page; p++){
                        $('.' + settings.divClass).eq(p-1).find('li.item').show();
                        requestQueue.push(p);
                    }

                    requestQueue.push(page);
                }
            },

            updateHash: function(offset, page){
                if(canUpdateHash){
                    window.location.hash = '#!offset=' + offset + '&p=' + page;
                }
            },

            calculateCurrentPage: function(offset){
                for(var p=pagesOffset.length-1; p>=0; p--){
                    if(offset >= pagesOffset[p]){
                        return p+1;
                    }
                }
                return 1;
            },

            updateRequestQueue: function(page){
                //if the page is not in request queue and has not been requested, put it in request queue
                if($.inArray(page, requestQueue) == -1 && $.inArray(page, requestedPages) == -1){
                    requestQueue.push(page);
                }
            },

            loadContent: function(){
                if(!methods.getCanLoadContent()){
                    return;
                }
                if(requestQueue.length == 0){
                    return;
                }
                var page = requestQueue[0];
                if(page == 1){
                    requestQueue.shift();
                }
                if(page && page != 1 && $.inArray(page, requestedPages) == -1){
                    requestedPages.push(page);
                    var requestUrl = location.href.replace(/&?is_mrnav=1/g, '');
                    $.ajax({
                        type: 'get',
                        url: requestUrl,
                        data: {"p" : page, "mr_scroll" : 1},
                        dataType: 'json',
                        success: function(data){
                            if(settings.keepBlockHeight){
                                if(page != pageCount){//don't keep last page
                                    var oldHeight = $('.' + settings.divClass).eq(page-1).height();
                                    $('.' + settings.divClass).eq(page-1).replaceWith(data.page);
                                    $('.' + settings.divClass).eq(page-1).height(oldHeight);
                                }
                                else{
                                    $('.' + settings.divClass).eq(page-1).replaceWith(data.page);
                                }
                            }
                            else{
                                $('.' + settings.divClass).eq(page-1).replaceWith(data.page);
                            }
                            
                            //after load callback
                            if (settings.afterLoad != null){
                                settings.afterLoad();
                            }
                            requestQueue.shift();
                            if(settings.keepBlockHeight){
                                if(requestQueue.length == 0){
                                    $('.' + settings.divClass).not('.fake').css('height', 'auto');
                                }
                            }
                            methods.loadContent();
                        }
                    });

                    //put all pages less than current request page to request queue
                    for(var p=page-1; p>=2; p--){
                        methods.updateRequestQueue(p);
                    }
                }
            },
            
            getPageCount: function(){
                var selector = 'div.pager p.amount';
                var pager = $(selector);
                if (pager.get(0)) {
                    var str = pager.get(0).innerHTML;
                    var re = /\D+(\d+)\D+(\d+)\D+(\d+)/;
                    var result = re.exec(str);
                    if (result && result.length == 4) {
                        if (result[3] > 0 && result[2] > 0) {
                            return Math.ceil(parseInt(result[3])/parseInt(result[2]));
                        }
                    }
                }
                return 1;
            },

            getTotalCount: function(){
                var selector = 'div.pager p.amount';
                var pager = $(selector);
                if (pager.get(0)) {
                    var str = pager.get(0).innerHTML;
                    var re = /\D+(\d+)\D+(\d+)\D+(\d+)/;
                    var result = re.exec(str);
                    if (result && result.length == 4) {
                        return result[3];
                    }
                }
            },

            getLimit: function(){
                var selector = 'div.pager p.amount';
                var pager = $(selector);
                if (pager.get(0)) {
                    var str = pager.get(0).innerHTML;
                    var re = /\D+(\d+)\D+(\d+)\D+(\d+)/;
                    var result = re.exec(str);
                    if (result && result.length == 4) {
                        return parseInt(result[2]);
                    }
                }
            },

            getColumnCount: function(){
                if(settings.mode == 'list'){
                    return 1;
                }
                else{
                    return $('.' + settings.divClass).children('ul').first().children('li').length;
                }
            },

            reInit: function(){
                $(".toolbar-bottom, .pager").hide();
                methods.removeSorterOnChangeAttribute();
                pageCount = methods.getPageCount();
                totalCount = methods.getTotalCount();
                limit = methods.getLimit();
                columnCount = methods.getColumnCount();
                requestedPages = [];
                requestQueue = [];
                pagesOffset = [];

                if(pageCount < 2){
                    return;
                }

                imagesLoaded($('.' + settings.divClass).get(0), function() {
                    methods.createFakeElements();
                    methods.initPagesOffset();
                    methods.initMoreButton();
                    methods.initFirstPage();
                });
            },

            reInitPagesOffset : function(){
                pagesOffset = [];
                methods.setFakeSectionHeight();
                methods.resetSectionHeight();
                methods.initPagesOffset();
            }
        }
        
        methods.init();

        return {
            reInit: methods.reInit,
            reInitPagesOffset : methods.reInitPagesOffset
        }
    };
    
    $.mrscroll.defaults = {
        mode          : 'grid', // grid or list
        afterLoad     : null,   // method called after load content
        divClass      : 'category-products', // category products block class
        moreBtnHtml   : '<button  id="mr-scroll-more" type="button" class="button"><span><span>Load More</span></span></button>',
        autoLoad      : false, // auto load or press button to load
        oneColButton  : null,  // id of changing to one column button
        twoColButton  : null,  // id of changing to two columns button
        loaderHtml    : '<p class="mr-scroll-loading">Loading<span>.</span><span>.</span><span>.</span></p>',
        keepBlockHeight : true // keep block height after loading content, which helps keep items not to move up and down after going backing from product page to a deep position
    };
})(jQuery);